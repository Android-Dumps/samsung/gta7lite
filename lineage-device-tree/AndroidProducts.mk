#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_gta7lite.mk

COMMON_LUNCH_CHOICES := \
    lineage_gta7lite-user \
    lineage_gta7lite-userdebug \
    lineage_gta7lite-eng
